import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import NavBar from "./components/navBar";
import Lane from "./components/lane";
import Placeholder from "./components/placeholder";
import FilterItem from "./components/filterItem";
import { setLaneFilter, fetchPostsWithRedux, searchFilter } from "./actions";
import Search from "./components/search";

const mapStateToProps = state => ({
  lanes: state.lanes.data || [],
  filter: state.laneFilter,
  searchFilter: state.searchFilter
});

const mapDispatchToProps = dispatch => ({
  getData: () => dispatch(fetchPostsWithRedux()),
  setFilter: filter => dispatch(setLaneFilter(filter)),
  handleSearch: value => dispatch(searchFilter(value))
});

class App extends Component {
  state = {
    outbid: 0,
    watching: 0,
    winning: 0,
    filterOptions: [],
    openSubMenu: false,
    selected: false
  };

  componentDidMount() {
    this.props.getData().then(() => this.buildFilterOptions());
  }

  getBadgeValue = () => {
    const lanes = this.props.lanes;
    return lanes.forEach(lane => {
      if (lane.isOutbid) {
        this.setState({
          outbid: this.state.outbid + 1
        });
      } else if (lane.isWatching) {
        this.setState({
          watching: this.state.watching + 1
        });
      } else if (lane.isWinning) {
        this.setState({
          winning: this.state.winning + 1
        });
      }
    });
  };

  buildFilterOptions = () => {
    this.getBadgeValue();
    const filterOptions = this.state.filterOptions;
    const { outbid, watching, winning } = this.state;
    filterOptions.push(
      { type: "SHOW_ALL", label: "All" },
      { type: "SHOW_OUTBID", label: "Outbid", badges: outbid > 0 && outbid },
      {
        type: "SHOW_WATCHING",
        label: "Watching",
        badges: watching > 0 && watching
      },
      { type: "SHOW_WINNING", label: "Winning", badges: winning > 0 && winning }
    );
    this.setState({ filterOptions });
  };

  getLaneData = () => {
    const {
      lanes,
      searchFilter,
      filter: { type, label }
    } = this.props;
    let filtered = lanes;
    if (searchFilter !== "") {
      filtered = lanes.filter(lane =>
        lane.make.toLowerCase().includes(searchFilter.toLowerCase())
      );
    } else {
      if (type === "SHOW_ALL") {
        filtered = lanes;
      } else {
        filtered = lanes.filter(lane => lane[`is${label}`]);
      }
    }
    return filtered;
  };

  toggleSubmenu = () => {
    this.setState({
      openSubMenu: !this.state.openSubMenu,
      selected: !this.state.selected
    });
  };

  handleFilter = filterOption => {
    return this.props.searchFilter !== ""
      ? this.state.filterOptions
      : this.props.setFilter(filterOption);
  };

  render() {
    const { openSubMenu, toggleSubmenu } = this.state;
    let lanes = this.getLaneData();

    return (
      <Fragment>
        <header className="header">
          <NavBar openSubMenu={openSubMenu} toggleSubmenu={toggleSubmenu} />
        </header>
        <main className="main">
          <div className="filters-section">
            <Search onChange={this.props.handleSearch} />
            <div className="filters">
              {this.state.filterOptions.map(filterOption => (
                <FilterItem
                  label={filterOption.label}
                  value={filterOption.badges}
                  key={filterOption.label}
                  handleFilter={() => this.handleFilter(filterOption)}
                />
              ))}
            </div>
            <div className="results">
              {lanes.length !== 0 ? (
                lanes.map((lane, index) => <Lane lane={lane} key={index} />)
              ) : (
                <div className="lane">Sorry no items here.</div>
              )}
            </div>
          </div>
          <div className="main-section">
            <Placeholder />
            <Placeholder />
            <Placeholder />
            <Placeholder />
          </div>
        </main>
      </Fragment>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
