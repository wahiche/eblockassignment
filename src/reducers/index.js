import { combineReducers } from "redux";

const lanes = (state = {}, action) => {
  switch (action.type) {
    case "FETCH_REQUEST":
      return state;
    case "FETCH_SUCCESS":
      return { ...state, data: action.payload.data.data.lanes };
    default:
      return state;
  }
};

const initialState = {
  type: "SHOW_ALL",
  label: "All"
};

const laneFilter = (state = initialState, action) => {
  switch (action.type) {
    case "SET_LANE_FILTER":
      return action.filter;
    default:
      return state;
  }
};

const searchFilter = (state = "", action) => {
  switch (action.type) {
    case "SEARCH_FILTER":
      return action.value;
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  lanes,
  laneFilter,
  searchFilter
});

export default rootReducer;
