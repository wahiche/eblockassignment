import axios from "axios";

export const fetchPostsRequest = () => {
  return {
    type: "FETCH_REQUEST"
  };
};

export const fetchPostsSuccess = payload => {
  return {
    type: "FETCH_SUCCESS",
    payload
  };
};

export const fetchPostsError = () => {
  return {
    type: "FETCH_ERROR"
  };
};

export const fetchPostsWithRedux = () => {
  return dispatch => {
    dispatch(fetchPostsRequest());
    return fetchPosts().then(response => {
      if (response.status === 200) {
        dispatch(fetchPostsSuccess(response));
      } else {
        dispatch(fetchPostsError());
      }
    });
  };
};

function fetchPosts() {
  const URL = "/data/payload.json";
  return axios.get(URL);
}

export const searchFilter = value => ({
  type: "SEARCH_FILTER",
  value
});

export const LaneFilters = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_OUTBID: "SHOW_OUTBID",
  SHOW_WATCHING: "SHOW_WATCHING",
  SHOW_WINNING: "SHOW_WINNING"
};

export const setLaneFilter = filter => ({
  type: "SET_LANE_FILTER",
  filter
});
