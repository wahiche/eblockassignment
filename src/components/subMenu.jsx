import React from "react";
import MenuItem from "./menuItem";

const SubMenu = ({ menuItems: items }) => {
  return items.map(item => (
    <MenuItem
      label={item}
      key={item}
      selected={item.toLowerCase() === "live lanes" ? "selected" : ""}
    />
  ));
};

export default SubMenu;
