import React from "react";

const Search = ({ value, onChange }) => {
  return (
    <div className="search">
      <input
        type="text"
        name="query"
        placeholder="Search Live Lanes"
        value={value}
        onChange={e => onChange(e.currentTarget.value)}
      />
    </div>
  );
};

export default Search;
