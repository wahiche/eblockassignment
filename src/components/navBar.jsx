import React from "react";
import Menu from "./menu";
import SubMenu from "./subMenu";
const buySubMenu = [
  "upcoming",
  "live appraisals",
  "live 24h auctions",
  "live lanes"
];
const mainMenuItems = [
  { icon: "list", label: "buy" },
  { icon: "directions_car", label: "sell" },
  { icon: "timer", label: "pending" },
  { icon: "check_circle_outline", label: "finished" }
];

const NavBar = ({ toggleSubmenu, selected, openSubMenu }) => {
  return (
    <nav className="navbar">
      <div className="main-menu">
        <div className="logo" />
        <Menu
          mainMenuItems={mainMenuItems}
          toggleSubmenu={toggleSubmenu}
          selected={selected}
        />
      </div>
      {selected && (
        <div className="sub-menu">
          {openSubMenu && (
            <SubMenu menuItems={buySubMenu} selected="live lanes" />
          )}
        </div>
      )}
    </nav>
  );
};

export default NavBar;
