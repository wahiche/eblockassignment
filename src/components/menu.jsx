import React, { Component } from "react";
import MenuItem from "./menuItem";

class Menu extends Component {
  state = {};

  render() {
    const { selected, toggleSubmenu } = this.props;
    return (
      <div className="menu">
        {this.props.mainMenuItems.map(menuItem => {
          const { icon, label } = menuItem;
          return (
            <MenuItem
              icon={icon}
              label={label}
              key={icon}
              selected={selected}
              onClick={toggleSubmenu}
            />
          );
        })}
      </div>
    );
  }
}

export default Menu;
