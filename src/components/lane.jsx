import React from "react";

const Lane = ({ lane: data }) => {
  const {
    lane,
    auction,
    itemsRemaining,
    itemsWatching,
    year,
    make,
    model,
    itemsWatching: isLive
  } = data;

  const details = `${year} ${make} ${model} ${isLive ? "is live" : ""}`;
  const Stock = () => {
    return (
      <div className="stock">
        <div className="item-remaining">
          {itemsRemaining ? `${itemsRemaining} items remaining. ` : ""}
        </div>
        <div className="item-watching">
          {itemsWatching ? ` ${itemsWatching} are on your watchlist` : ""}
        </div>
      </div>
    );
  };
  return (
    <div className="lane">
      <div className="titles">
        <div className="title">{lane}</div>
        <div className="auction">{auction}</div>
      </div>
      <div className="details">{details}</div>
      <Stock />
    </div>
  );
};

export default Lane;
