import React from "react";

const FilterItem = ({ label, value, handleFilter }) => {
  return (
    <div className="filter" onClick={handleFilter}>
      {label}
      {value && <div className={`badge ${label.toLowerCase()}`}>{value}</div>}
    </div>
  );
};

export default FilterItem;
