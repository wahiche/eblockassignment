import React from "react";

const Placeholder = () => {
  return (
    <div className="placeholder">
      <div className="image" />
      <div className="price" />
      <div className="text">
        <div className="text-block" />
        <div className="text-block" />
        <div className="text-block" />
      </div>
    </div>
  );
};

export default Placeholder;
