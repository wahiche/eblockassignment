import React from "react";

const MenuItem = ({ icon, label, onClick, selected }) => {
  return (
    <div
      id={icon}
      className={`menu-item ${selected ? "selected" : ""}`}
      onClick={onClick}
    >
      {icon && <i className="material-icons menu-icon">{icon}</i>}
      <div className="menu-label">{label}</div>
    </div>
  );
};

export default MenuItem;
